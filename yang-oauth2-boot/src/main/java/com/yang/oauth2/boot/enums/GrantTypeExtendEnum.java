package com.yang.oauth2.boot.enums;

import lombok.Getter;
import org.yang.common.core.enums.IBaseEnum;

import java.io.Serializable;

/**
 * 授权类型扩展枚举
 *
 * @author: lslands
 * @version: 1.0
 * @create: 2024/5/12
 * @description:
 */
@Getter
public enum GrantTypeExtendEnum implements Serializable {
    /**
     * 密码模式
     */
    PASSWORD("password")
    ;

    private final String grantType;

    GrantTypeExtendEnum(String grantType) {
        this.grantType = grantType;
    }


}
