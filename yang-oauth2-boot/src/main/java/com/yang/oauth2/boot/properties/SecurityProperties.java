package com.yang.oauth2.boot.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * securiy 动态参数属性
 *
 * @author: lslands
 * @version: 1.0
 * @create: 2024/5/12
 * @description:
 */
@Configuration
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    /**
     * security放行白名单
     * @date 2024/5/12 14:00
     */
    private List<String> whitelistPaths = new ArrayList<>();

    public List<String> getWhitelist() {
        return whitelistPaths;
    }
    public void setWhitelist(List<String> whitelistPaths) {
        this.whitelistPaths=whitelistPaths;
    }

}
