package com.yang.oauth2.boot.service.userDetils;

import com.yang.oauth2.boot.model.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.yang.common.core.enums.UserStatusEnum;
import org.yang.common.core.model.UserInfo;

import java.util.*;

/**
 * 自定认证实现
 *
 * @author: lslands
 * @version: 1.0
 * @create: 2024/5/12
 * @description:
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String pwd = passwordEncoder.encode("admin123");
        List<String> root = new ArrayList<>();
        root.add("root");
        List<String> perms = new ArrayList<>();
        perms.add("add");
        UserInfo admin = UserInfo.builder()
                .userId(1L)
                .nickname("章撒")
                .avatar("").username("admin").password(pwd).companyId(1L)
                .roles(root).perms(perms).enabled(UserStatusEnum.ONLINE).build();
        return new MyUserDetails(admin);
    }
}
