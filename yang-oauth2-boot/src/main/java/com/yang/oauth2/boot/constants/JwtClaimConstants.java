package com.yang.oauth2.boot.constants;

/**
 * JWT 自定义字段
 * @author: lslands
 * @description:
 * @version：v1.0
 * @date: 2024/6/3 12:13
 */
public interface JwtClaimConstants {


    /**
     * 用户ID
     */
    String USER_ID = "userId";

    /**
     * 用户名
     */
    String USERNAME = "username";

    /**
     * 昵称
     */
    String NICKNAME="nickname";

    /**
     * 头像
     */
    String AVATAR="avatar";

    /**
     * 企业ID
     */
    String COMPANY_ID = "companyId";

    /**
     * 状态
     */
    String ENABLED ="enabled";

    /**
     * 权限(角色Code)集合
     */
    String AUTHORITIES = "authorities";

}
