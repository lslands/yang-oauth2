package com.yang.oauth2.boot.service.oidc;

import com.yang.oauth2.boot.model.CustomOidcUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.yang.common.core.enums.UserStatusEnum;
import org.yang.common.core.model.UserInfo;

import java.util.*;

/**
 * 自定义 OIDC 用户信息服务
 * @author: lslands
 * @description:
 * @version：v1.0
 * @date: 2024/6/3 15:59
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CustomOidcUserInfoService {

    private final PasswordEncoder passwordEncoder;
    public CustomOidcUserInfo loadUserByUsername(String username) {
        UserInfo userInfo = null;
        try {
//            userInfo = userFeignClient.getUserAuthInfo(username);
            String pwd = passwordEncoder.encode("admin123");
            List<String> root = new ArrayList<>();
            root.add("root");
            List<String> perms = new ArrayList<>();
            perms.add("add");
            userInfo= UserInfo.builder()
                    .userId(1L)
                    .nickname("章撒")
                    .avatar("").username("admin").password(pwd).companyId(1L)
                    .roles(root).perms(perms).enabled(UserStatusEnum.ONLINE).build();
            if (userInfo == null) {
                return null;
            }
            return new CustomOidcUserInfo(createUser(userInfo));
        } catch (Exception e) {
            log.error("获取用户信息失败", e);
            return null;
        }
    }

    private Map<String, Object> createUser(UserInfo userInfo) {
        return CustomOidcUserInfo.customBuilder()
                .username(userInfo.getUsername())
                .nickname(userInfo.getNickname())
                .status(userInfo.getEnabled().getValue())
                .phoneNumber("15928310871")
                .email("1430536748@qq.com")
                .profile("https://www.qq.com")
                .build()
                .getClaims();
    }


}
