package com.yang.oauth2.boot.security;

import cn.hutool.core.collection.CollectionUtil;
import com.yang.oauth2.boot.properties.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import java.util.List;

/**
 * @author: lslands
 * @version: 1.0
 * @create: 2024/5/12
 * @description:
 */
@Slf4j
@EnableWebSecurity
@Configuration(proxyBeanMethods = false)
public class SecurityConfig  extends SecurityProperties {


    /**
     * 用户安全策略
     * @param http http
     * @param introspector 入参
     * @return SecurityFilterChain
     * @author lslands
     * @date 2024/5/12 14:06
     */
    @Bean
    @Order(2)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http, HandlerMappingIntrospector introspector)
            throws Exception {
        //Mvc请求匹配器
        MvcRequestMatcher.Builder mvcMatcherBuilder = new MvcRequestMatcher.Builder(introspector);
        http.authorizeHttpRequests((requests) -> {
                            if (CollectionUtil.isNotEmpty(super.getWhitelist())) {
                                for (String whitelistPath : super.getWhitelist()) {
                                    requests.requestMatchers(mvcMatcherBuilder.pattern(whitelistPath)).permitAll();
                                }
                            }
                            requests.anyRequest().authenticated();
                        }
                )
                .csrf(AbstractHttpConfigurer::disable)
                .formLogin(Customizer.withDefaults());
        log.info("Seurity拦截路由:{}",super.getWhitelist());
        return http.build();
    }
}
