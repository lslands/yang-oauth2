package com.yang.oauth2.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2BootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2BootApplication.class, args);
    }

}
