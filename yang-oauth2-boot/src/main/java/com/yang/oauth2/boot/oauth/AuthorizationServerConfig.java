package com.yang.oauth2.boot.oauth;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import com.yang.oauth2.boot.model.MyUserDetails;
import com.yang.oauth2.boot.oauth.handler.Oauth2FailureHandler;
import com.yang.oauth2.boot.oauth.handler.Oauth2SuccessHandler;
import com.yang.oauth2.boot.oauth.extend.password.PasswordAuthenticationConverter;
import com.yang.oauth2.boot.oauth.extend.password.PasswordAuthenticationProvider;
import com.yang.oauth2.boot.oauth.jackson.MyUserMixin;
import com.yang.oauth2.boot.oauth.oidc.CustomOidcAuthenticationConverter;
import com.yang.oauth2.boot.oauth.oidc.CustomOidcAuthenticationProvider;
import com.yang.oauth2.boot.service.oidc.CustomOidcUserInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.bootstrap.encrypt.KeyProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.jackson2.SecurityJackson2Modules;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.jwt.JwtDecoder;

import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.jackson2.OAuth2AuthorizationServerJackson2Module;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.token.*;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 授权服务配置
 *
 * @author: lslands
 * @version: 1.0
 * @create: 2024/6/3
 * @description:
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class AuthorizationServerConfig {

    /**
     * 自定义授权页
     */
    private static final String CUSTOM_CONSENT_PAGE_URI = "/oauth2/consent";
    /**
     * 自定义登录页
     */
    private static final String CUSTOM_LOGIN_PAGE_URI = "";

    private final OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer;
    private final CustomOidcUserInfoService customOidcUserInfoService;
    private final JdbcTemplate jdbcTemplate;

    /**
     * 注册客户端，查寻数据库 oauth2_registered_client
     *
     * @return RegisteredClientRepository 注册的客户端存储库
     * @author lslands
     * @date 2024/5/12 13:39
     */
    @Bean
    public RegisteredClientRepository registeredClientRepository() {
        return new JdbcRegisteredClientRepository(jdbcTemplate);
    }

    /**
     * 令牌发放记录 (oauth2_authorization_consent)
     * @return OAuth2AuthorizationConsentService
     */
    @Bean
    public OAuth2AuthorizationConsentService authorizationConsentService() {
        return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository());
    }
    /**
     * 授权记录
     * @param registeredClientRepository registeredClientRepository
     * @return OAuth2AuthorizationService
     * @author lslands
     * @date 2024/6/3 11:10
     */
    @Bean
    public OAuth2AuthorizationService authorizationService(RegisteredClientRepository registeredClientRepository) {
        //解决spring security oauth2 自定义user 用户登录 Jackson报错
        // 创建基于JDBC的OAuth2授权服务。这个服务使用JdbcTemplate和客户端仓库来存储和检索OAuth2授权数据。
        JdbcOAuth2AuthorizationService service = new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
        // 创建并配置用于处理数据库中OAuth2授权数据的行映射器。
        JdbcOAuth2AuthorizationService.OAuth2AuthorizationRowMapper rowMapper = new JdbcOAuth2AuthorizationService.OAuth2AuthorizationRowMapper(registeredClientRepository);
        rowMapper.setLobHandler(new DefaultLobHandler());
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = JdbcOAuth2AuthorizationService.class.getClassLoader();
        List<Module> securityModules = SecurityJackson2Modules.getModules(classLoader);
        objectMapper.registerModules(securityModules);
        objectMapper.registerModule(new OAuth2AuthorizationServerJackson2Module());
        // 添加自定义Mixin，用于序列化/反序列化特定的类。
        // Mixin类需要自行实现，以便Jackson可以处理这些类的序列化。
        objectMapper.addMixIn(MyUserDetails.class, MyUserMixin.class);
        objectMapper.addMixIn(Long.class, Object.class);
        // 将配置好的ObjectMapper设置到行映射器中。
        rowMapper.setObjectMapper(objectMapper);
        // 将自定义的行映射器设置到授权服务中。
        service.setAuthorizationRowMapper(rowMapper);
        return service;
    }


    /**
     * 授权服务器端点配置(安全过滤链)
     *
     * @param http HttpSecurity
     * @return SecurityFilterChain
     * @author lslands
     * @date 2024/5/12 15:06
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http, AuthenticationManager authenticationManager, OAuth2AuthorizationService authorizationService, OAuth2TokenGenerator<?> tokenGenerator)
            throws Exception {
        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);
        http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
                .authorizationEndpoint(authorizationEndpoint -> authorizationEndpoint.consentPage(CUSTOM_CONSENT_PAGE_URI))

                //令牌端点
                .tokenEndpoint((tokenEndpoint) -> tokenEndpoint
                        //自定义授权模式转换器(Converter)
                        .accessTokenRequestConverters(authenticationConverters ->
                                authenticationConverters.addAll(
                                        List.of(
                                                new PasswordAuthenticationConverter()
                                        )
                                )
                        )
                        //自定义授权模式提供者(Provider)
                        .authenticationProviders(authenticationProviders ->
                                authenticationProviders.addAll(
                                        List.of(
                                                new PasswordAuthenticationProvider(authenticationManager, authorizationService, tokenGenerator)
                                        )
                                )
                        )
                        .accessTokenResponseHandler(new Oauth2SuccessHandler()) // 自定义成功响应
                        .errorResponseHandler(new Oauth2FailureHandler()) // 自定义失败响应
                )
                // Enable OpenID Connect 1.0 自定义
                .oidc(Customizer.withDefaults())
//                .oidc(oidcCustomizer ->
//                        oidcCustomizer.userInfoEndpoint(userInfoEndpointCustomizer ->
//                                {
//                                    userInfoEndpointCustomizer.userInfoRequestConverter(new CustomOidcAuthenticationConverter(customOidcUserInfoService));
//                                    userInfoEndpointCustomizer.authenticationProvider(new CustomOidcAuthenticationProvider(authorizationService));
//                                }
//                        )
//                )
        ;
        http.csrf(AbstractHttpConfigurer::disable)
                .exceptionHandling((exceptions) -> exceptions
                        .defaultAuthenticationEntryPointFor(
                                new LoginUrlAuthenticationEntryPoint("/login"),
                                new MediaTypeRequestMatcher(MediaType.TEXT_HTML)
                        )
                )
                // 接受用户信息和/或客户端注册的访问令牌
                .oauth2ResourceServer((resourceServer) -> resourceServer
                        .jwt(Customizer.withDefaults()));

        return http.build();
    }


    @Bean
    OAuth2TokenGenerator<?> tokenGenerator(JWKSource<SecurityContext> jwkSource) {
        JwtGenerator jwtGenerator = new JwtGenerator(new NimbusJwtEncoder(jwkSource));
        jwtGenerator.setJwtCustomizer(jwtCustomizer);
        OAuth2AccessTokenGenerator accessTokenGenerator = new OAuth2AccessTokenGenerator();
        OAuth2RefreshTokenGenerator refreshTokenGenerator = new OAuth2RefreshTokenGenerator();
        return new DelegatingOAuth2TokenGenerator(
                jwtGenerator, accessTokenGenerator, refreshTokenGenerator);
    }


    /**
     * jwt加密
     * @return JWKSource<SecurityContext>
     * @author lslands
     * @date 2024/5/12 13:49
     */
    @Bean
    public JWKSource<SecurityContext> jwkSource() {
        KeyPair keyPair = generateRsaKey();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAKey rsaKey = new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(UUID.randomUUID().toString())
                .build();
        JWKSet jwkSet = new JWKSet(rsaKey);
        return new ImmutableJWKSet<>(jwkSet);
    }
    @Bean
    public KeyProperties keyProperties(){
        return new KeyProperties();
    }
    private KeyPair generateRsaKey() {
        KeyPair keyPair;
        try {
            KeyProperties.KeyStore keyStore = keyProperties().getKeyStore();
            KeyStoreKeyFactory factory = new KeyStoreKeyFactory(keyStore.getLocation(),
                    keyStore.getSecret().toCharArray());
            keyPair=factory.getKeyPair(keyStore.getAlias(), keyStore.getPassword().toCharArray());
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
        return keyPair;
    }

    @Bean
    public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
        return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
    }
    @Bean
    public AuthorizationServerSettings authorizationServerSettings() {
        return AuthorizationServerSettings.builder().build();
    }
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }


}
